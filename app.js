var fs = require('fs');
var CsvReadableStream = require('csv-reader');
var CreateXML = require('./create-xml');

var inputStream = fs.createReadStream('catalog-cross.csv', 'utf8');
var data = {};
inputStream.pipe(CsvReadableStream({ delimiter: ';',parseNumbers: true, parseBooleans: true, trim: true }))
    .on('data', function (row) {
        // Create master
        var master = row[0];
        if(master === 'MASTER ID' || !master) return;
        var variationGroup = row[1];
        
        // Create variation group
        if(!data[master]) data[master] = {};
        if(row[10]) data[master].name = row[10];
        
        var masterInstance = data[master];
        
        // Up selling
        if(row[21]) masterInstance.upSelling = row[21];
        if(row[22]) masterInstance.crossSelling = row[22];

        if(variationGroup) {
            if(!masterInstance[variationGroup]) masterInstance[variationGroup] = {};
            vGroupInstance = masterInstance[variationGroup];
            // master images
            if(row[5]) {
                vGroupInstance.images = [];

                var imageList = row[5].replace(/(\r\n|\n|\r)/gm,"").split(',');
                vGroupInstance.images = [];
                // If it's a variation product image put an object instead of an img
                imageList.forEach(element => {
                    if(row[2]) {
                        vGroupInstance.images.push({image:element,shade: row[2]});
                    } else {
                        vGroupInstance.images.push(element);
                    }
                });
            }
            
            
            // Internal name 
            if(row[8]) vGroupInstance.internalName = row[8];
            // Name
            if(row[10]) vGroupInstance.name = row[10];
            // Brand // BRAND is 9
            if(row[9]) vGroupInstance.brand = row[9];
            // Description
            if(row[11]) vGroupInstance.description = row[11];
            // Long description
            if(row[12]) vGroupInstance.longDescription = row[12];
            // Category is 16
            if(row[16]) vGroupInstance.category = row[16];
            // Concern filter
            if(row[17]) vGroupInstance.concern = row[17];
            // Skin type
            if(row[18]) vGroupInstance.skinType = row[18];
            // Application area
            if(row[19]) vGroupInstance.applicationArea = row[19];
            // Textures
            if(row[20]) vGroupInstance.textures = row[20];
            // New
            if(row[23] && row[23] == 'VERDADERO') vGroupInstance.new = true;
            else vGroupInstance.featured = false;

            // SEO
            // Meta title
            if(row[24]) vGroupInstance.metaTitle = row[25];
            // Meta description
            if(row[25]) vGroupInstance.metaDescription = row[26];
            // Meta keywords
            if(row[26]) vGroupInstance.metaKeywords = row[27];
            // Shade
            if(row[2]) vGroupInstance.isdin_shade = row[2];
            
            // Create variation products
            if(row[3]) {
                if(!vGroupInstance.variants) vGroupInstance.variants = {};
                vGroupInstance.variants[row[3]] = {};
                var variant = vGroupInstance.variants[row[3]]
                // Shade
                if(row[2]) variant['isdin_shade'] = row[2];
                // Price
                if(row[13]) variant['price'] = row[13];
                // Size
                if(row[4]) variant['size'] = row[4];
                // Weight
                if(row[15]) variant['weight'] = row[15];
            }
        }
    })
    .on('end', () => {
        CreateXML.generate(data);
    });
