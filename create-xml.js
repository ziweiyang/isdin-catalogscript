var fs = require("fs");
var n = 0;
var CreateXML = {
    generate(data) {
        const masterName = 'isdin_master-catalog.xml';
        const storefrontName = 'isdin-us_storefront-catalog.xml';
        fs.writeFile('data.json', JSON.stringify(data), function (err) {
            if (err) throw err;
            console.log('JSON data created without errors');
        });
        // Generate the master catalog file
        CreateXML.createMaster(masterName, data);
        CreateXML.createStorefront(storefrontName, data);
        CreateXML.createPricebook('isdin-pricebook.xml',data);
    },
    createMaster(fileName, data) {
        const catalogName = 'isdin_master-catalog';
        var result = `<?xml version="1.0" encoding="UTF-8"?>
            <catalog xmlns="http://www.demandware.com/xml/impex/catalog/2006-10-31" catalog-id="`+ catalogName + `">
                <header>
                    <image-settings>
                        <internal-location base-path="/"/>
                        <view-types>
                            <view-type>large</view-type>
                            <view-type>medium</view-type>
                            <view-type>small</view-type>
                            <view-type>swatch</view-type>
                            <view-type>hi-res</view-type>
                        </view-types>` +
            '<alt-pattern>${productname}</alt-pattern>' + // Using single quotes as it breaks with ${} syntax
            '<title-pattern>${productname}</title-pattern>' +
            `</image-settings>
                </header>
                
                <category category-id="root">
                    <online-flag>true</online-flag>
                    <position>0.0</position>
                    <template/>
                    <page-attributes/>
                    <refinement-definitions>
                        <refinement-definition type="attribute" bucket-type="none" attribute-id="isdin_indication" system="false">
                            <display-name xml:lang="x-default">Concern</display-name>
                            <value-set>search-result</value-set>
                            <sort-mode>value-name</sort-mode>
                            <sort-direction>ascending</sort-direction>
                            <cutoff-threshold>5</cutoff-threshold>
                        </refinement-definition>
                        <refinement-definition type="attribute" bucket-type="none" attribute-id="isdin_skin_type" system="false">
                            <display-name xml:lang="x-default">Skin type</display-name>
                            <value-set>search-result</value-set>
                            <sort-mode>value-name</sort-mode>
                            <sort-direction>ascending</sort-direction>
                            <cutoff-threshold>5</cutoff-threshold>
                        </refinement-definition>
                        <refinement-definition type="attribute" bucket-type="none" attribute-id="brand" system="true">
                            <display-name xml:lang="x-default">Brand</display-name>
                            <value-set>search-result</value-set>
                            <sort-mode>value-name</sort-mode>
                            <sort-direction>ascending</sort-direction>
                            <cutoff-threshold>5</cutoff-threshold>
                        </refinement-definition>
                    </refinement-definitions>
                </category>`;
        for (let i in data) {
            var row = data[i];
            var masterProduct = CreateXML.createProduct('master',i,row);
            result += masterProduct;
            for (let k in row) {
                if (typeof row[k] == 'object') {
                    var variationGroup = CreateXML.createProduct('variationGroup', k, row[k]);
                    result += variationGroup;
                    if (row[k].variants) {
                        for (let j in row[k].variants) {
                            row[k].variants[j].name = row[k].name;
                            var variationProduct = CreateXML.createProduct('variant', j, row[k].variants[j]);
                            result += variationProduct;
                        }
                    }
                }
            }
        }
        result += `<variation-attribute attribute-id="isdin_shade" variation-attribute-id="isdin_shade">
                <display-name xml:lang="x-default">Shade</display-name>
                <variation-attribute-values>
                    <variation-attribute-value value="Sand">
                        <display-value xml:lang="x-default">Sand</display-value>
                    </variation-attribute-value>
                    <variation-attribute-value value="Bronze">
                        <display-value xml:lang="x-default">Bronze</display-value>
                    </variation-attribute-value>
                    <variation-attribute-value value="Caramel">
                        <display-value xml:lang="x-default">Caramel</display-value>
                    </variation-attribute-value>
                </variation-attribute-values>
            </variation-attribute>`
        result += `<variation-attribute attribute-id="isdin_size" variation-attribute-id="isdin_size">
            <display-name xml:lang="x-default">Size</display-name>
            <variation-attribute-values>
                <variation-attribute-value value="15 ml">
                    <display-value xml:lang="x-default">15 ml</display-value>
                </variation-attribute-value>
                <variation-attribute-value value="30 ml">
                    <display-value xml:lang="x-default">30 ml</display-value>
                </variation-attribute-value>
                <variation-attribute-value value="50 ml">
                    <display-value xml:lang="x-default">50 ml</display-value>
                </variation-attribute-value>
                <variation-attribute-value value="75 ml">
                    <display-value xml:lang="x-default">75 ml</display-value>
                </variation-attribute-value>
                <variation-attribute-value value="100 ml">
                    <display-value xml:lang="x-default">100 ml</display-value>
                </variation-attribute-value>
                <variation-attribute-value value="125 ml">
                    <display-value xml:lang="x-default">125 ml</display-value>
                </variation-attribute-value>
                <variation-attribute-value value="400 ml">
                    <display-value xml:lang="x-default">400 ml</display-value>
                </variation-attribute-value>
            </variation-attribute-values>
        </variation-attribute>`;
        result += '</catalog>';

        fs.writeFile(fileName, result, function (err) {
            if (err) throw err;
            console.log('Master created without errors');
        });
    },
    createProduct(type, id, row) {
        var productTemplate = `<product product-id="${id}">
            <ean/>
            <upc/>
            <unit/>
            <min-order-quantity>1</min-order-quantity>
            <step-quantity>1</step-quantity>
                <display-name xml:lang="x-default">${row.name}</display-name>`;
        if (type === 'variationGroup') {
            productTemplate += `<short-description xml:lang="x-default">${row.description}</short-description>`;
            productTemplate += `<long-description xml:lang="x-default">${row.longDescription.replace('&', 'and')}</long-description>`;
        }

        productTemplate += `<online-flag>true</online-flag>
                <available-flag>false</available-flag>`;

        // Make the variation groups searchable
        if(type === 'variationGroup') {
            productTemplate += `<searchable-flag>true</searchable-flag>`;
        } else {
            productTemplate += `<searchable-flag>false</searchable-flag>`;
        }
        // Include images
        if (type === 'master') {
            productTemplate += `<images>\n`;
            var images = {
                default: []
            };

            for(let i in row) {
                if(row[i].images) {
                    // Set images for variation products
                    for (let k in row[i].images) {
                        var img = row[i].images[k];
                        if(typeof img === 'object') {
                            if(!images[img.shade]) images[img.shade] = [];
                            images[img.shade].push(img.image);
                        } else {
                            images.default.push(row[i].images[k])
                        }
                    }
                }
            }
            // In case it has image groups
            if (Object.keys(images).length > 1) {
                Object.keys(images).forEach(element => {
                    if(images[element].length) {
                        var imgs = images[element];
                        productTemplate += `<image-group view-type="hi-res">
                        <variation attribute-id="isdin_shade" value="${element}"/>`
                        imgs.forEach(el=>{
                            productTemplate += `<image path="hi-res/${el}"/>`
                        })
                        productTemplate += `</image-group>`;
                    }
                });
            } else {
                productTemplate += `<image-group view-type="hi-res">`;
                images.default.forEach(element => {
                    productTemplate += `<image path="hi-res/${element}"/>`
                });
                productTemplate += `</image-group>\n`;
            }
            productTemplate += `</images>\n`;
        }
        if (type === 'variationGroup') {
            // Brand
            if (row.brand) productTemplate += `<brand>${row.brand.trim()}</brand>`;
        }

        // Page attributes
        productTemplate += `<page-attributes>\n`;
        if (type == 'variationGroup') {
            /* SEO Metadata */
            if (row.metaTitle) productTemplate += `<page-title xml:lang="x-default">${row.metaTitle}</page-title>`
            if (row.metaDescription) productTemplate += `<page-description xml:lang="x-default">${row.metaDescription.replace('&','and')}</page-description>`
            if (row.metaKeywords) productTemplate += `<page-keywords xml:lang="x-default">${row.metaKeywords.replace('&', 'and')}</page-keywords>`
        }
        productTemplate += `</page-attributes>\n`;
        /* Custom attributes */
        productTemplate += `<custom-attributes>\n`;
        if (type == 'variationGroup') {
            // Internal name
            if (row.internalName) productTemplate += `<custom-attribute attribute-id="isdin_internalName">${row.internalName}</custom-attribute>`;
            // Application area
            if (row.applicationArea) productTemplate += `<custom-attribute attribute-id="isdin_application_area">${row.applicationArea.replace('&', 'and')}</custom-attribute>`;
            // Featured
            if (row.featured) {
                productTemplate += `<custom-attribute attribute-id="isdin_featured">true</custom-attribute>`;
            } else {
                productTemplate += `<custom-attribute attribute-id="isdin_featured">false</custom-attribute>`;
            }
            // Concern
            if (row.concern) {
                productTemplate += `<custom-attribute attribute-id="isdin_indication" xml:lang="x-default">\n`;
                let item = row.concern.split(',');
                for (let value in item) productTemplate += '<value>' + item[value].trim() + '</value>\n';
                productTemplate += `</custom-attribute>`;
            }

            /**
             * Seems like the catalog removed this catalog attribute and now is a category
             */
            // Best seller
            // if (row.bestSeller) {
            //     productTemplate += `<custom-attribute attribute-id="isdin_bestSeller">true</custom-attribute>`;
            // } else {
            //     productTemplate += `<custom-attribute attribute-id="isdin_bestSeller">false</custom-attribute>`;
            // }

            // Skin type
            if (row.skinType) productTemplate += `<custom-attribute attribute-id="isdin_skin_type">${row.skinType}</custom-attribute>`;
            // Textures
            if (row.textures) productTemplate += `<custom-attribute attribute-id="isdin_textures">${row.textures}</custom-attribute>`;
        }

        if (type == 'variant') {
            // Size
            if (row.size) productTemplate += `<custom-attribute attribute-id="isdin_size">${row.size}</custom-attribute>`;
            if (row.weight) productTemplate += `<custom-attribute attribute-id="isdin_weight">${String(row.weight).replace(',', '.')}</custom-attribute>`;
            if (row.isdin_shade) productTemplate += `<custom-attribute attribute-id="isdin_shade">${row.isdin_shade}</custom-attribute>`;
        }
        if (type == 'variationGroup') if (row.isdin_shade) productTemplate += `<custom-attribute attribute-id="isdin_shade">${row.isdin_shade}</custom-attribute>`;

        productTemplate += `</custom-attributes>\n`;
        if (type == 'master') {
            productTemplate += `<variations>\n`;
            var variants = [];
            var variations = [];
            for (let variationGroup in row) {
                var variation = row[variationGroup];
                if (typeof variation == 'object' && variationGroup != 'images' && variationGroup !='name') {
                    
                    variations.push(variationGroup);
                    for (const key of Object.keys(variation.variants)) {
                        variants.push(key);
                    }
                }
            }
            productTemplate += `<attributes>
                    <shared-variation-attribute attribute-id="isdin_shade" variation-attribute-id="isdin_shade"/>
                    <shared-variation-attribute attribute-id="isdin_size" variation-attribute-id="isdin_size"/>
                </attributes>`;
            productTemplate += `<variants>`;
            for (let i in variants) {
                productTemplate += `<variant product-id="${variants[i]}"/>\n`;
            }
            productTemplate += `</variants>`;
            productTemplate += `<variation-groups>`;
            for (let i in variations) {
                productTemplate += `<variation-group product-id="${variations[i]}"/>`;
            }
            productTemplate += `</variation-groups>
                </variations>\n`;
            if(row.upSelling || row.crossSelling) {
                productTemplate += `<product-links>`;
                if(row.upSelling) {
                    var items = row.upSelling.split(',');
                    items.forEach(elements=>{
                        productTemplate += `<product-link product-id="${elements.trim()}" type="up-sell"/>`
                    });
                }
                if(row.crossSelling) {
                    var crossItems = row.crossSelling.split(',');
                    crossItems.forEach(elements=>{
                        productTemplate += `<product-link product-id="${elements.trim()}" type="cross-sell"/>`
                    })
                }
                productTemplate += `</product-links>`;
            }
        }

        productTemplate += `<pinterest-enabled-flag>false</pinterest-enabled-flag>
            <facebook-enabled-flag>false</facebook-enabled-flag>
            <store-attributes>
                <force-price-flag>false</force-price-flag>
                <non-inventory-flag>false</non-inventory-flag>
                <non-revenue-flag>false</non-revenue-flag>
                <non-discountable-flag>false</non-discountable-flag>
            </store-attributes>
        </product>`;
        return productTemplate;
    },
    createStorefront(fileName, data) {
        var template = `<?xml version="1.0" encoding="UTF-8"?>
<catalog xmlns="http://www.demandware.com/xml/impex/catalog/2006-10-31" catalog-id="isdin-us_storefront-catalog">
    <header>
        <image-settings>
            <internal-location base-path="/"/>
            <view-types>
                <view-type>large</view-type>
                <view-type>medium</view-type>
                <view-type>small</view-type>
                <view-type>swatch</view-type>
                <view-type>hi-res</view-type>
            </view-types>` +
            '<alt-pattern>${productname}</alt-pattern>' + // Using single quotes as it breaks with ${} syntax
            '<title-pattern>${productname}</title-pattern>' +
            `</image-settings>
    </header>

    <category category-id="root">
        <online-flag>true</online-flag>
        <position>0.0</position>
        <template/>
        <page-attributes/>
    </category>

    <category category-id="skincare">
        <display-name xml:lang="x-default">Skincare</display-name>
        <online-flag>true</online-flag>
        <parent>root</parent>
        <position>1.0</position>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_categoryMenuBanner" xml:lang="x-default">&lt;div class="submenu-banner-image-wrapper"&gt;&#13;
							&lt;img src="$include('Page-IncludeDISImage', 'name', skincare-min.png, 'width', 270, 'height', 405, 'mode', 'cut','urlMode','true')$" alt="submenu-image"&gt;&#13;
						&lt;/div&gt;&#13;
						&lt;p class="submenu-banner-description"&gt;&#13;
							New in!&lt;br/&gt;&#13;
							Isdinceutics Flavo-C Ultraglican&#13;
						&lt;/p&gt;</custom-attribute>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">false</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="antiaging">
        <display-name xml:lang="x-default">Anti-aging</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <position>1.0</position>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">false</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="body-care">
        <display-name xml:lang="x-default">Body care</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="cleansers">
        <display-name xml:lang="x-default">Cleansers</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">false</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="eye-treatments">
        <display-name xml:lang="x-default">Eye treatments</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="foundation">
        <display-name xml:lang="x-default">Foundation</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="hair-thinning">
        <display-name xml:lang="x-default">Hair thinning</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">false</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="moisturizers">
        <display-name xml:lang="x-default">Moisturizers</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="skincare-ampoules-and-serums">
        <display-name xml:lang="x-default">Ampoules &amp; Serums</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="sunprotection">
        <display-name xml:lang="x-default">Sun protection</display-name>
        <online-flag>true</online-flag>
        <parent>skincare</parent>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="ampoules-and-serums">
        <display-name xml:lang="x-default">Ampoules &amp; Serums</display-name>
        <online-flag>true</online-flag>
        <parent>root</parent>
        <position>2.0</position>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="sun-protection">
        <display-name xml:lang="x-default">Sun protection</display-name>
        <online-flag>true</online-flag>
        <parent>root</parent>
        <position>3.0</position>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">true</custom-attribute>
        </custom-attributes>
    </category>

    <category category-id="best-sellers">
        <display-name xml:lang="x-default">Best sellers</display-name>
        <online-flag>true</online-flag>
        <parent>root</parent>
        <position>4.0</position>
        <template/>
        <page-attributes/>
        <custom-attributes>
            <custom-attribute attribute-id="isdin_showInMenu">true</custom-attribute>
            <custom-attribute attribute-id="isdin_showProductsInMenu">true</custom-attribute>
        </custom-attributes>
    </category>`;

        var products = [];
        for (let i in data) {
            var master = data[i];
            for (let k in master) {
                if (k !== 'name') {
                    var variationGroup = master[k];
                    var category = variationGroup.category;
                    products.push({ id: k, cat: category });
                    for (let j in variationGroup.variants) {
                        products.push({ id: j, cat: category });
                    }
                }
            }
        }
        for (let i in products) {
            var item = products[i];
            if(item.id && item.cat) {
                
                var categories = item.cat.replace(/(\r\n|\n|\r)/gm,"").split(',')
                categories.forEach((category,index) => {
                    var catName = category.trim();
                    if(index === 0) {
                        template += `<category-assignment category-id="${catName}" product-id="${item.id}">
                            <primary-flag>true</primary-flag>
                        </category-assignment>`
                    } else {
                        template += `<category-assignment category-id="${catName}" product-id="${item.id}"/>`
                    }
                })
            }
        }

        template += '</catalog>';
        fs.writeFile(fileName, template, function (err) {
            if (err) throw err;
            console.log('Storefront created without errors');
        });
    },
    createPricebook(fileName,data) {
        var template = `<?xml version="1.0" encoding="UTF-8"?>
        <pricebooks xmlns="http://www.demandware.com/xml/impex/pricebook/2006-10-31">
            <pricebook>
                <header pricebook-id="isdin-pricebook">
                    <currency>USD</currency>
                    <display-name xml:lang="x-default">ISDIN US</display-name>
                    <online-flag>true</online-flag>
                </header>
                <price-tables>`;

        var products = [];
        for (let i in data) {
            var master = data[i];
            for (let k in master) {
                if (k !== 'name') {
                    var variationGroup = master[k];
                    for (let j in variationGroup.variants) {
                        products.push({ id: j, price: variationGroup.variants[j].price });
                    }
                }
            }
        }
        for (let i in products) {
            var item = products[i];
            if(item.id && item.price) {
                template += `<price-table product-id="${item.id}">
                    <amount quantity="1">${item.price.replace('$','')}</amount>
                </price-table>`
            }
        }
                
        template += `</price-tables>
            </pricebook>
        </pricebooks>`;

        fs.writeFile(fileName, template, function (err) {
            if (err) throw err;
            console.log('Pricebook created without errors');
        });
    }
}
module.exports = CreateXML